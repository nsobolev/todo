export const categoriesTypes = {
  setCategories: 'SET_CATEGORIES',
  addCategory: 'ADD_CATEGORY',
  deleteCategory: 'DELETE_CATEGORY',

  setSelectedCategory: 'SET_SELECTED_CATEGORY',
  editNameCategory: 'EDIT_NAME_CATEGORY',

  setTasks: 'SET_TASKS',
  addTask: 'ADD_TASK',
  deleteTask: 'DELETE_TASK',
  editTask: 'EDIT_TASK',
  changeCompleted: 'CHANGE_COMPLETED',
  filterTasksByCompleted: 'FILTER_BY_COMPLETED',
};
