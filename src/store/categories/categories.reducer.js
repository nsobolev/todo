import { categoriesTypes } from './categories.types';
import { listTasksPropertyUpdate, selectedCategoryTasksPropertyUpdate } from './utils/updates';

const initialState = {
  list: [],
  selectedCategory: null,
};

export const categoriesReducer = (state = initialState, action) => {
  switch (action.type) {
    case categoriesTypes.setCategories: {
      return {
        ...state,
        list: action.payload,
      }
    }

    case categoriesTypes.addCategory: {
      return {
        ...state,
        list: [ ...state.list, action.payload ],
      }
    }

    case categoriesTypes.deleteCategory: {
      return {
        ...state,
        list: state.list.filter(category => category.id !== action.payload),
      }
    }

    case categoriesTypes.setSelectedCategory: {
      return {
        ...state,
        selectedCategory: action.payload,
      }
    }

    case categoriesTypes.editNameCategory: {
      return {
        ...state,
        selectedCategory: { ...state.selectedCategory, name: action.payload.name },
        list: state.list.map(category => category.id === action.payload.id ? { ...category, name: action.payload.name } : category),
      }
    }

    case categoriesTypes.addTask: {
      return {
        ...state,
        selectedCategory: state.selectedCategory && state.selectedCategory.id === action.payload.categoryId ?
          { ...state.selectedCategory, tasks: [ ...state.selectedCategory.tasks, action.payload ] } :
          state.selectedCategory,
        list: state.list.map( category => category.id === action.payload.categoryId ?
          { ...category, tasks: [ ...category.tasks, action.payload ] } :
          category),
      }
    }

    case categoriesTypes.deleteTask: {
      return {
        ...state,
        selectedCategory: state.selectedCategory && state.selectedCategory.id === action.payload.categoryId ?
          { ...state.selectedCategory, tasks: state.selectedCategory.tasks.filter(task => task.id !== action.payload.taskId) } :
          state.selectedCategory,
        list: state.list.map( category => category.id === action.payload.categoryId ?
          { ...category, tasks: category.tasks.filter(task => task.id !== action.payload.taskId) } :
          category),
      }
    }

    case categoriesTypes.editTask: {
      return {
        ...state,
        selectedCategory: selectedCategoryTasksPropertyUpdate(state, action, 'text'),
        list: listTasksPropertyUpdate(state, action, 'text'),
      }
    }

    case categoriesTypes.changeCompleted: {
      return {
        ...state,
        selectedCategory: selectedCategoryTasksPropertyUpdate(state, action, 'completed'),
        list: listTasksPropertyUpdate(state, action, 'completed')
      }
    }

    case categoriesTypes.filterTasksByCompleted: {
      return {
        ...state,
        selectedCategory: state.selectedCategory && state.selectedCategory.id === action.payload.categoryId ?
          { ...state.selectedCategory, tasks: [ ...action.payload.tasks ] } :
          state.selectedCategory,
        list: state.list.map(category => category.id === action.payload.categoryId ?
          { ...category, tasks: [ ...action.payload.tasks ] } : category)
      }
    }

    case categoriesTypes.setTasks: {
      return {
        ...state,
        selectedCategory: state.selectedCategory && state.selectedCategory.id === action.payload.categoryId ?
          { ...state.selectedCategory, tasks: [ ...action.payload.tasks ] } :
          state.selectedCategory,
        list: state.list.map(category => category.id === action.payload.categoryId ?
          { ...category, tasks: [ ...action.payload.tasks ] } :
          category)
      }
    }

    default:
      return state;
  }
};
