import { categoriesTypes } from './categories.types';

export const setCategories = (categories) => ({ type: categoriesTypes.setCategories, payload: categories  });
export const setNewCategory = (category) => ({ type: categoriesTypes.addCategory, payload: category });
export const setDeleteCategory = (id) => ({ type: categoriesTypes.deleteCategory, payload: id });

export const setSelectedCategory = (category) => ({ type: categoriesTypes.setSelectedCategory, payload: category });
export const setNewNameCategory = (id, name) => ({ type: categoriesTypes.editNameCategory, payload: { id, name } });

export const setAddTask = (task) => ({ type: categoriesTypes.addTask, payload: task });
export const setDeleteTask = (taskId, categoryId) => ({ type: categoriesTypes.deleteTask, payload: { taskId, categoryId } });
export const setNewNameTask = (taskId, categoryId, data) => ({ type: categoriesTypes.editTask, payload: { taskId, categoryId, data } });
export const setChangeCompleted = (taskId, categoryId, data) => ({ type: categoriesTypes.changeCompleted, payload: { taskId, categoryId, data }});
export const setFilterTasksByCompleted = (categoryId, tasks) => ({ type: categoriesTypes.filterTasksByCompleted, payload: { categoryId, tasks } });
export const setTasksForCategory = (categoryId, tasks) => ({ type: categoriesTypes.setTasks, payload: { categoryId, tasks } });

