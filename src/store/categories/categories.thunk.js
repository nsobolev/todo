import { todoApi } from '../../api/todoApi';
import {
  setCategories, setSelectedCategory,
  setNewCategory, setDeleteCategory, setNewNameCategory,
  setAddTask, setDeleteTask, setNewNameTask, setChangeCompleted,
  setFilterTasksByCompleted, setTasksForCategory,
} from './categories.actions';

export const addCategory = (category, colorName) => (dispatch) => {
  todoApi.categories.addCategory(category)
    .then(data => {
      const dataToState = {...data, color: { name: colorName }};

      dispatch(setNewCategory(dataToState));
    });
};

export const deleteCategory = (id) => (dispatch) => {
  todoApi.categories.deleteCategory(id)
    .then(() => {
      dispatch(setDeleteCategory(id));
    });
};

export const getCategories = () => (dispatch) => {
  todoApi.categories.getCategories()
    .then(categories => {
      dispatch(setCategories(categories));
    })
};

export const getCategory = (id) => (dispatch) => {
  todoApi.categories.getCategory(id)
    .then(category => {
      dispatch(setSelectedCategory(category));
    })
};

export const editNameCategory = (id, name) => (dispatch) => {
  todoApi.categories.editNameCategory(id, name)
    .then(() => {
      dispatch(setNewNameCategory(id, name));
    })
};

export const addTask = (task) => (dispatch) => {
  todoApi.tasks.addTask(task)
    .then((data) => {
      const taskToState = { ...task, id: data.id };
      dispatch(setAddTask(taskToState));
    });
};

export const deleteTask = (taskId, categoryId) => (dispatch) => {
  todoApi.tasks.deleteTask(taskId)
    .then(() => {
      dispatch(setDeleteTask(taskId, categoryId));
    });
};

export const editNameTask = (taskId, categoryId, taskText) => (dispatch) => {
  todoApi.tasks.editNameTask(taskId, taskText)
    .then(() => {
      dispatch(setNewNameTask(taskId, categoryId, taskText));
    });
};

export const changeCompletedTask = (taskId, categoryId, completed) => (dispatch) => {
  todoApi.tasks.changeCompleted(taskId, completed)
    .then( () => {
     dispatch(setChangeCompleted(taskId, categoryId, completed));
    })
};

export const filterTasksByCompleted = (categoryId, completed) => (dispatch) => {
  todoApi.tasks.filterTasksByCompleted(categoryId, completed)
    .then((tasks) => {
      dispatch(setFilterTasksByCompleted(categoryId, tasks));
    });
};

export const getTasksForCategory = (categoryId) => (dispatch) => {
  todoApi.tasks.getTasksForCategory(categoryId)
    .then((tasks) => {
      dispatch(setTasksForCategory(categoryId, tasks));
    });
};
