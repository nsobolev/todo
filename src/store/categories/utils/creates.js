export const createCategory = (name, colorId) => ({
  name,
  colorId,
  tasks: [],
});

export const createTask = (categoryId, text, completed) => ({
  categoryId,
  text,
  completed,
});
