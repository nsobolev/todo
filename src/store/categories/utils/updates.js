export const listTasksPropertyUpdate = (state, action, property) => {
  return state.list.map(category => category.id === action.payload.categoryId ?
    { ...category, tasks: category.tasks.map(task => task.id === action.payload.taskId ?
        { ...task, [property]: action.payload.data } : task )} :
    category)
};

export const selectedCategoryTasksPropertyUpdate = (state, action, property) => {
  return state.selectedCategory && state.selectedCategory.id === action.payload.categoryId ?
    { ...state.selectedCategory, tasks: state.selectedCategory.tasks.map(task => task.id === action.payload.taskId ?
        { ...task, [property]: action.payload.data } : task) } :
    state.selectedCategory;
};
