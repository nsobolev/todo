import { combineReducers } from 'redux';

import { categoriesReducer } from './categories/categories.reducer';
import { colorsReducer } from './colors/colors.reducer';


const reducer = combineReducers({
  categories: categoriesReducer,
  colors: colorsReducer,
});

export default reducer;
