import { colorsTypes } from './colors.types';

const initialState = {
  list: [],
};

export const colorsReducer = (state = initialState, action) => {
  switch (action.type) {
    case colorsTypes.setColors: {
      return {
        ...state,
        list: action.payload,
      }
    }

    default:
      return state;
  }
};
