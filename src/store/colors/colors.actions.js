import { colorsTypes } from './colors.types';

export const setColors = (colors) => ({ type: colorsTypes.setColors, payload: colors });
