import { todoApi } from '../../api/todoApi';
import { setColors } from './colors.actions';

export const getColors = () => (dispatch) => {
  todoApi.colors.getColors()
    .then(data => {
      dispatch(setColors(data))
    })
};
