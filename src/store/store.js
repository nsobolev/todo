import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

import reducer from './reducer';

const middleWares = (process.env.NODE_ENV === 'development') ? [logger, thunk] : [thunk];

const store = createStore(reducer, applyMiddleware(...middleWares));

export default store;
