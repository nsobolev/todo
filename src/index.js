import React from 'react';
import ReactDOM from 'react-dom';
import store from './store/store';

import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';

import App from './components/App';

const root = document.getElementById('root');

ReactDOM.render(
  <Router>
    <Provider store={ store }>
      <App />
    </Provider>
  </Router>,
  root
);
