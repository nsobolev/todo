import React from 'react';
import classNames from "classnames";

const FormTextArea = ({ name, value, className, ...inputProps  }) => {
  return (
    <textarea
      className={classNames('form__textarea', { [className]: Boolean(className) })}
      id={ name }
      value={ value }
      { ...inputProps }
    />
  )
};

FormTextArea.defaultProps = {
  required: false,
  className: '',
  onChange: () => {},
  placeholder: '',
};

export default FormTextArea;
