import Form from './Form.jsx';
import FormField from './FormField';
import FormInput from './FormInput';
import FormTextArea from './FormTextArea';

export default Form;

export {
  FormField,
  FormInput,
  FormTextArea,
}
