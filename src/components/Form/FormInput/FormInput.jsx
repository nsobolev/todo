import React from 'react';
import classNames from 'classnames';

const FormInput = ({ name, value, className, ...inputProps }) => {
  return (
    <input
      className={classNames('form__input', { [className]: Boolean(className) })}
      id={ name }
      value={ value }
      { ...inputProps }
    />
  )
};

FormInput.defaultProps = {
  type: 'text',
  required: false,
  className: '',
  onChange: () => {},
  placeholder: '',
};

export default FormInput;
