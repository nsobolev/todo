import React from 'react';
import classNames from 'classnames';

const Form = ({ children, onSubmit, isRow = false }) => {
  return (
    <form className={ classNames('form', { 'form_view_row': isRow }) } onSubmit={ onSubmit }>
      { children }
    </form>
  )
};

export default Form;
