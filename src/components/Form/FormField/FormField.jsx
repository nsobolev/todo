import React from 'react';
import classNames from 'classnames';

const FormField = ({ className = '', children }) => {
  return (
    <div className={classNames('form__field', { [className]: Boolean(className) })}>
      { children }
    </div>
  )
};

export default FormField;
