import React from 'react';
import classNames from 'classnames';

const Button = ({ className = '', title = '', type = 'button', onClick = () => {}, disabled = false }) => {
  return (
    <button className={ classNames('button', { [className]: Boolean(className) }) }
            type={ type }
            onClick={ onClick }
            disabled={ disabled }
    >
      <span className="button__title">{ title }</span>
    </button>
  )
};

export default Button;
