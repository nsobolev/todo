import React from 'react';
import classNames from 'classnames';

const ButtonAction = ({ Icon, onClick = () => {}, isSizeMiddle = false, isSizeBig = false }) => {
  return (
    <button
      className={ classNames(
        'button-action',
        { 'button-action_size_middle': isSizeMiddle },
        { 'button-action_size_big': isSizeBig },
      )}
      type="button"
      onClick={ onClick }
    >
      <Icon className="button-action__icon" />
    </button>
  )
};

export default ButtonAction;
