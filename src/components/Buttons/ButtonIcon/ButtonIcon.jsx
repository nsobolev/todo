import React, { forwardRef } from 'react';
import classNames from 'classnames';

const ButtonIcon = forwardRef(({ Icon, title, type, onClick, isActive, isHoverText }, ref) => {
  return (
    <button
      className={
        classNames('button-icon',
          { 'button-icon_state_active': isActive },
          { 'button-icon_view_hover-color': isHoverText })
      }
      type={ type }
      onClick={ onClick }
      ref={ ref }
    >
      <Icon className="button-icon__icon" />
      <span className="button-icon__title">{ title }</span>
    </button>
  )
});

ButtonIcon.defaultProps = {
  title: '',
  type: 'button',
  onClick: () => {},
  isActive: false,
  isHoverText: false,
};

export default ButtonIcon;
