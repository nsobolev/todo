import React from 'react';
import { Link } from 'react-router-dom';

const Welcome = () => {
  return (
    <section className="welcome">
      <div className="welcome__content">
        <h2 className="welcome__slogan">Заметки</h2>
        <p className="welcome__title">Выберите папку или посмотрите все задачи</p>
        <div className="welcome__actions">
          <Link className="button button_view_main welcome__button" to="/categories">Перейти ко всем задачам</Link>
        </div>
      </div>
    </section>
  );
};

export default Welcome;
