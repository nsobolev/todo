import React, { useEffect } from 'react';

import { connect } from 'react-redux';
import Tasks from './Tasks.jsx';
import { useParams } from 'react-router-dom';

import { getCategory, editNameCategory,
  addTask, deleteTask, editNameTask, changeCompletedTask,
  filterTasksByCompleted, getTasksForCategory,
} from '../../store/categories/categories.thunk';

const TasksContainer = (props) => {
  const { id } = useParams();
  const { categories, getCategory, ...tasksProps } = props;

  useEffect(() => {
    if (id) {
      getCategory(id);
    }
  }, [id]);

  if (!id && categories.length) {
    return categories.map(category => (
      <div className="wrapper__content-item" key={ category.id }>
        <Tasks { ...tasksProps }
               category={ category }
        />
      </div>
    ));
  }

  if (tasksProps.category) {
    return <Tasks { ...tasksProps } />
  }

  return (
    <div>
      Loading..
    </div>
  )
};

const mapStateToProps = ({ categories }) => {
  return {
    categories: categories.list,
    category: categories.selectedCategory,
  }
};

const mapDispatchToProps = {
  getCategory,
  editNameCategory,
  addTask,
  deleteTask,
  editNameTask,
  changeCompletedTask,
  filterTasksByCompleted,
  getTasksForCategory
};

export default connect(mapStateToProps, mapDispatchToProps)(TasksContainer);
