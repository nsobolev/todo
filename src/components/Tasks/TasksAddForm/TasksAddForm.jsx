import React, { useState } from 'react';

import Form, { FormField, FormTextArea } from '../../Form';
import { Button, ButtonIcon } from '../../Buttons';
import { createTask } from '../../../store/categories/utils/creates';

import PlusSVG from '../../../svg/plus.svg';

const TasksAddForm = ({ categoryId, addTask }) => {
  const [ isVisibleForm, setIsVisibleForm ] = useState(false);
  const [ taskText, setTaskText ] = useState('');

  const toggleVisibleForm = () => {
    setIsVisibleForm(!isVisibleForm);
  };

  const onSubmitForm = (evt) => {
    evt.preventDefault();

    const newTask = createTask(categoryId, taskText, false);

    addTask(newTask);
    toggleVisibleForm();
    setTaskText('');
  };

  const onChangeTextArea = (evt) => {
    const { value } = evt.target;

    setTaskText(value);
  };

  return (
    <div className="tasks-add-form">
      {
        isVisibleForm ? (
          <Form onSubmit={ onSubmitForm }>
            <FormField>
              <FormTextArea value={ taskText } onChange={ onChangeTextArea } />
            </FormField>
            <FormField>
              <Button type="submit" title="Добавить" className="button_view_main" disabled={ !Boolean(taskText.length) } />
              <Button type="button" title="Отмена" className="button_view_secondary" onClick={ toggleVisibleForm } />
            </FormField>
          </Form>
        ) : (
          <ButtonIcon Icon={ PlusSVG } title="Новая задача" onClick={ toggleVisibleForm } isHoverText />
        )
      }
    </div>
  )
};

export default TasksAddForm;
