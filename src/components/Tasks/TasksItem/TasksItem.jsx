import React, { useState } from 'react';
import classNames from 'classnames';

import { ButtonAction } from '../../Buttons';
import TaskEditItemName from '../TaskEditItemName';

import CloseSVG from '../../../svg/close.svg';
import EditSVG from '../../../svg/edit.svg';
import CheckSVG from '../../../svg/check.svg';

const TasksItem = ({ id, categoryId, text = '', deleteTask = () => {}, editNameTask = () => {}, changeCompletedTask = () => {}, completed = false }) => {
  const [ isEditName, setIsEditName ] = useState(false);

  const toggleIsEdit = () => {
    setIsEditName(!isEditName);
  };

  const onChangeCheckbox = (evt) => {
    const { checked } = evt.target;

    changeCompletedTask(id, categoryId, checked);
  };

  return (
    <div className="tasks-item">
      <div className="tasks-item__content">
        <label className={ classNames('tasks-item__label', { 'tasks-item__label_state_checked': completed }) }>
          <input className="tasks-item__checkbox visually-hidden" onChange={ onChangeCheckbox } type="checkbox" checked={ completed } />
          <div className="tasks-item__indicator">
            <CheckSVG className="tasks-item__indicator-icon" />
          </div>
          { !isEditName && text }
        </label>
        <TaskEditItemName
          id={ id }
          categoryId={ categoryId }
          name={ text }
          editNameTask={ editNameTask }
          isEdit={ isEditName }
          setIsEdit={ setIsEditName }
        />
      </div>
      <div className="tasks-item__actions">
        <div className="tasks-item__action">
          <ButtonAction
            Icon={ EditSVG }
            onClick={ toggleIsEdit }
            isSizeMiddle
          />
        </div>
        <div className="tasks-item__action">
          <ButtonAction
            Icon={ CloseSVG }
            isSizeMiddle
            onClick={ () => deleteTask(id, categoryId) }
          />
        </div>
      </div>
    </div>
  )
};

export default TasksItem;
