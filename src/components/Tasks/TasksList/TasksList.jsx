import React from 'react';

import TasksItem from '../TasksItem';

const TasksList = ({ category, deleteTask, editNameTask, changeCompletedTask }) => {
  return (
    <ul className="tasks-list">
      {
        category.tasks.map(task => (
          <li key={ task.id } className="tasks-list__item">
            <TasksItem { ...task } deleteTask={ deleteTask } editNameTask={ editNameTask } changeCompletedTask={ changeCompletedTask } />
          </li>
        ))
      }
    </ul>
  )
};

export default TasksList;
