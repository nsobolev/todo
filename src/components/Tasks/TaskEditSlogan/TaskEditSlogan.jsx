import React, { useEffect, useState } from 'react';
import classNames from 'classnames';

import { ButtonAction } from "../../Buttons";
import EditValue from '../../EditValue';
import { Link } from 'react-router-dom';

import EditSVG from "../../../svg/edit.svg";

const TaskEditSlogan = ({ id, slogan = '', classNameSlogan, onSubmitSlogan = () => {} }) => {
  const [ isEditSlogan, setIsEditSlogan] = useState(false);
  const [ valueSlogan, setValueSlogan ] = useState(slogan);

  useEffect(() => {
    setIsEditSlogan(false);
    setValueSlogan(slogan);
  }, [slogan]);

  const onSubmit = () => {
    onSubmitSlogan(id, valueSlogan);
  };

  return (
    <EditValue value={ valueSlogan } originalValue={ slogan } setValue={ setValueSlogan }
               isEdit={ isEditSlogan } setIsEdit={ setIsEditSlogan }
               onSubmitValue={ onSubmit }
    >
      <div className="edit-slogan">
        <h2 className={ classNames('edit-slogan__slogan', { [classNameSlogan]: Boolean(classNameSlogan) }) }>
          <Link className="edit-slogan__link" to={`/categories/${id}`}>
            { slogan }
          </Link>
        </h2>
        <div className="edit-slogan__button-container">
          <ButtonAction Icon={ EditSVG } onClick={ () => setIsEditSlogan(true) }  isSizeBig />
        </div>
      </div>
    </EditValue>
  )
};

export default TaskEditSlogan;
