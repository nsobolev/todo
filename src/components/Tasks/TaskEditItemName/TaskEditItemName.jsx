import React, { useState, useEffect } from 'react';

import EditValue from '../../EditValue';

const TaskEditItemName = ({ id, categoryId, name, isEdit, setIsEdit, editNameTask }) => {
  const [ valueName, setValueName ] = useState(name);

  useEffect(() => {
    setIsEdit(false);
    setValueName(name);
  }, [name]);

  const onSubmit = () => {
    editNameTask(id, categoryId, valueName);
  };

  return (
    <EditValue value={ valueName } originalValue={ name } setValue={ setValueName }
               isEdit={ isEdit } setIsEdit={ setIsEdit }
               onSubmitValue={ onSubmit } />
  );
};

export default TaskEditItemName;
