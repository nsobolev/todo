import React from 'react';

import TasksList from './TasksList';
import TasksAddForm from './TasksAddForm';
import TaskEditSlogan from './TaskEditSlogan';
import Select from '../Select';

const Tasks = ({ category, editNameCategory, addTask, deleteTask, editNameTask, changeCompletedTask, filterTasksByCompleted, getTasksForCategory }) => {
  const getTasks = () => {
    getTasksForCategory(category.id);
  };

  const filterByCompleted = (completed) => {
    filterTasksByCompleted(category.id, completed);
  };

  return (
    <section className="tasks">
      <header className="tasks__header">
        <TaskEditSlogan
          id={ category.id }
          slogan={ category.name }
          onSubmitSlogan={ editNameCategory }
          classNameSlogan={ `${category.color.name}_color` }
        />
        <div className="tasks__filter">
          <Select
            options={[
              { value: 'true', label: 'Выполненные' },
              { value: 'false', label: 'Не выполненные' },
            ]}
            onChangeOptionAction={ filterByCompleted }
            onDropOptionAction={ getTasks }
            placeholder="Сортировка"
            key={ category.id }
          />
        </div>
      </header>
      <div className="tasks__content">
        <div className="tasks__list">
          <TasksList category={ category } deleteTask={ deleteTask } editNameTask={ editNameTask } changeCompletedTask={ changeCompletedTask } />
        </div>
        <div className="tasks__form">
          <TasksAddForm
            key={ category.id }
            categoryId={ category.id }
            addTask={ addTask }
          />
        </div>
      </div>
    </section>
  )
};

export default Tasks;
