import React, { forwardRef } from 'react';

import { useHistory } from 'react-router-dom';
import ListCategory from '../Category/ListCategory/ListCategoryContainer';
import Popover from '../Popover';
import { ButtonIcon } from '../Buttons';
import AddCategory from '../Category/AddCategory';

import BurgerSVG from '../../svg/burger-svg.svg';
import PlusSVG from '../../svg/plus.svg';

const Sidebar = () => {
  const history = useHistory();

  return (
    <div className="sidebar">
      <div className="sidebar__header">
        <ButtonIcon
          Icon={ BurgerSVG }
          title="Все задачи"
          onClick={ () => history.push('/categories') }
        />
      </div>
      <div className="sidebar__list">
        <ListCategory isRemovable />
      </div>
      <div className="sidebar__footer">
        <Popover
          AreaClick={
            forwardRef((props, ref) => (
              <ButtonIcon ref={ ref } onClick={ props.onClick } Icon={ PlusSVG } title="Добавить задачу" isHoverText/>
            ))
          }
        >
          <AddCategory />
        </Popover>
      </div>
    </div>
  );
};

export default Sidebar;
