import React, { useState } from 'react';
import { connect } from 'react-redux';
import ListCategory from './ListCategory.jsx';

import { getCategories, deleteCategory } from '../../../store/categories/categories.thunk';

const ListCategoryContainer = ({ categories, selectedCategory, getCategories, deleteCategory, isRemovable }) => {
  useState(() => {
    getCategories();
  }, []);

  return (
    <ListCategory
      categories={ categories }
      selectedCategory={ selectedCategory }
      deleteCategory={ deleteCategory }
      isRemovable={ isRemovable }
    />
  )
};

const mapStateToProps = ({ categories }) => {
  return {
    categories: categories.list,
    selectedCategory: categories.selectedCategory,
}};

const mapDispatchToProps = {
  getCategories,
  deleteCategory,
};

export default connect(mapStateToProps, mapDispatchToProps)(ListCategoryContainer);
