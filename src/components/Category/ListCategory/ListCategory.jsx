import React from 'react';
import CategoryItem from '../CategoryItem';
import { NavLink } from 'react-router-dom';

const ListCategory = ({ categories, selectedCategory, isRemovable = false, deleteCategory = () => {} }) => {
  console.log(categories);
  return (
    <ul className="list-category">
        {
          categories.map(category => (
            <li className="list-category__item" key={ category.id }>
              <NavLink className="list-category__link" activeClassName="list-category__link_state_active" to={`/categories/${category.id}`}>
                <CategoryItem
                  { ...category }
                  countTasks={ category.tasks.length }
                  deleteCategory={ () => deleteCategory(category.id) }
                  isSelected={ selectedCategory && category.id === selectedCategory.id }
                  isRemovable={ isRemovable }
                />
              </NavLink>
            </li>
          ))
        }
    </ul>
  )
};

export default ListCategory;
