import React, { useState, useEffect } from 'react';

import { connect } from 'react-redux';
import { getColors } from '../../../store/colors/colors.thunk';
import { addCategory } from '../../../store/categories/categories.thunk';
import { createCategory } from '../../../store/categories/utils/creates';

import Form, { FormField, FormInput } from '../../Form';
import CircleList from '../../CircleList';
import { Button } from '../../Buttons';

const AddCategory = ({ colors, getColors, addCategory }) => {
  const [ categoryName, setCategoryName ] = useState('');
  const [ categoryColorId, setCategoryColorId ] = useState(1);

  useEffect(() => {
    if (!colors.length) {
      getColors();
    } else {
      setCategoryColorId(colors[0].id);
    }
  }, [colors]);

  const onSubmitForm = (evt) => {
    evt.preventDefault();

    const newCategory = createCategory(categoryName, categoryColorId);

    addCategory(newCategory, colors.find(color => color.id === newCategory.colorId).name);
    resetForm();
  };

  const onChangeInput = (evt) => {
    const { value } = evt.target;

    setCategoryName(value);
  };

  const resetForm = () => {
    setCategoryName('');
    setCategoryColorId(colors[0].id);
  };

  return (
    <Form onSubmit={ onSubmitForm }>
      <FormField>
        <FormInput
          value={ categoryName }
          onChange={ onChangeInput }
          placeholder="Название папки"
        />
      </FormField>
      <FormField>
        <CircleList
          circles={ colors }
          onClickCircle={ setCategoryColorId }
          selectedColorId={ categoryColorId }
          isSizeBig />
      </FormField>
      <FormField>
        <Button type="submit" title="Добавить" className="button_view_main form__button-fluid" disabled={ !Boolean(categoryName.length) } />
      </FormField>
    </Form>
  )
};

const mapStateToProps = ({ colors }) => ({
  colors: colors.list,
});

const mapDispatchToProps = {
  getColors,
  addCategory,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddCategory);
