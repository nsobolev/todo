import React from 'react';

import Circle from '../../CircleList/Circle';
import { ButtonAction } from '../../Buttons';
import classNames from 'classnames';

import CloseSVG from '../../../svg/close.svg';

const CategoryItem = ({ color = {}, name = '', deleteCategory = () => {}, countTasks = 0, isSelected = false, isRemovable = false }) => {
  return (
    <div className={ classNames('category-item', { 'category-item_state_active': isSelected }) }>
      <div className="category-item__circle">
        <Circle colorName={ color.name } />
      </div>
      <p className="category-item__title">{ name }</p>
      <div className="category-item__count-tasks">({ countTasks })</div>
      {
        isRemovable &&
          <div className="category-item__button-delete">
            <ButtonAction onClick={ deleteCategory } Icon={ CloseSVG } />
          </div>
      }
    </div>
  )
};

export default CategoryItem;
