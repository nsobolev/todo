import React from 'react';
import Form, { FormField, FormInput } from "../Form";
import { Button } from "../Buttons";

const EditValue = ({ value, originalValue, setValue, isEdit, setIsEdit, onSubmitValue, children = null }) => {
  const onSubmitForm = (evt) => {
    evt.preventDefault();

    onSubmitValue();
  };

  const onChangeEdit = (evt) => {
    const { value } = evt.target;

    setValue(value);
  };

  const onCancelEdit = (evt) => {
    evt.stopPropagation();

    setValue(originalValue);
    setIsEdit(false);
  };

  return (
    isEdit ? (
      <div className="edit-value">
        <div className="edit-value__form">
          <Form onSubmit={ onSubmitForm } isRow >
            <FormField>
              <FormInput
                value={ value }
                onChange={ onChangeEdit }
                placeholder="Название папки"
              />
            </FormField>
            <FormField>
              <Button type="submit" title="Изменить" className="button_view_main" disabled={ !Boolean(value.length) } />
              <Button type="button" title="Отмена" className="button_view_secondary" onClick={ onCancelEdit } />
            </FormField>
          </Form>
        </div>
      </div> ) : children
  )
};

export default EditValue;
