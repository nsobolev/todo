import React from 'react';
import Circle from './Circle';

const CircleList = ({ circles, selectedColorId = 0, onClickCircle = () => {}, isSizeBig = false }) => {
  return (
    <ul className="circle-list">
      {
        circles.map(circle => (
          <li className="circle-list__item" key={ circle.id }>
            <Circle
              colorName={ circle.name }
              onClick={ () => onClickCircle(circle.id) }
              isSizeBig={ isSizeBig }
              isActive={ circle.id === selectedColorId }
            />
          </li>
        ))
      }
    </ul>
  )
};

export default CircleList;
