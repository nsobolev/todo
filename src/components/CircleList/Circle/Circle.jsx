import React from 'react';
import classNames from 'classnames';

const Circle = ({ colorName = '', onClick = () => {}, isSizeBig = false, isActive = false }) => {
  return (
    <div
      className={ classNames('circle',
      { [colorName]: Boolean(colorName) },
      { 'circle_size_big': isSizeBig },
      { 'circle_active': isActive }) }
      onClick={ onClick }
    />
  )
};

export default Circle;
