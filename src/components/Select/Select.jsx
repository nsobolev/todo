import React, { useState, useEffect } from 'react';

import SelectInput from 'react-select';
import { ButtonAction } from '../Buttons';

import CloseSVG from '../../svg/close.svg';

const Select = ({ options, placeholder, onChangeOptionAction, onDropOptionAction }) => {
  const [ selectedOption, setSelectedOption ] = useState(null);

  useEffect(() => {
    return () => {
      onDropOptionAction();
    };
  }, []);

  const onChangeOption = (option) => {
    const { value } = option;
    setSelectedOption(option);

    onChangeOptionAction(value);
  };

  const onDropOption = () => {
    setSelectedOption(null);

    onDropOptionAction();
  };

  return (
    <div className="select-container">
      <div className="select-container__select">
        <SelectInput
          placeholder={ placeholder }
          className="select"
          classNamePrefix="select"
          value={ selectedOption }
          onChange={ onChangeOption }
          options={ options }
        />
      </div>
      { selectedOption &&
        <div className="select-container__drop-filter">
          <ButtonAction Icon={ CloseSVG } isSizeMiddle onClick={ onDropOption } />
        </div>
      }
    </div>
  );
};

Select.defaultProps = {
  options: [],
  placeholder: 'Выберите элемент...',
};

export default Select;
