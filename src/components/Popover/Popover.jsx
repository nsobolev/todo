import React, { useState } from 'react';
import CloseSVG from '../../svg/close.svg';

import PopoverTiny from 'react-tiny-popover';

const Popover = ({ children, AreaClick }) => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <PopoverTiny
      isOpen={isOpen}
      position="bottom"
      align="start"
      containerClassName="popover"
      onClickOutside={ () => setIsOpen(false) }
      content={
        <div className="popover__content">
          <button className="popover__button-close" onClick={ () => setIsOpen(false) }>
            <CloseSVG className="popover__icon-close" />
          </button>
          { children }
        </div> }
    >
      {
        (ref) => (
          <AreaClick ref={ ref } onClick={ () => setIsOpen(!isOpen) } />
        )
      }
    </PopoverTiny>
  );
};

export default Popover;
