import React from 'react';
import '../../styles/bootstrap.less';

import { Switch, Route } from 'react-router-dom';
import Sidebar from '../Sidebar';
import Tasks from '../Tasks';
import Welcome from '../Welcome';

const App = () => {
  return (
    <div className="wrapper">
      <div className="wrapper__container container">
        <div className="wrapper__sidebar">
          <Sidebar />
        </div>
        <div className="wrapper__content">
          <Switch>
            <Route exact path="/" component={ Welcome } />
            <Route path="/categories/:id?" component={ Tasks } />
          </Switch>
        </div>
      </div>
    </div>
  )
};

export default App;
