import axios from 'axios';

const instanceApi = axios.create({
  baseURL: 'http://localhost:3000/',
});

export const todoApi = {
  categories: {
    getCategories() {
      return instanceApi.get('/categories?_expand=color&_embed=tasks').then(({ data }) => data);
    },

    getCategory(id) {
      return instanceApi.get(`categories/${id}/?_expand=color&_embed=tasks`).then(({ data }) => data);
    },

    addCategory(category) {
      return instanceApi.post('/categories', category).then(({ data }) => data);
    },

    deleteCategory(id) {
      return instanceApi.delete(`/categories/${id}`).then(({ data }) => data);
    },

    editNameCategory(id, name) {
      return instanceApi.patch(`/categories/${id}`, { name }).then(({ data }) => data);
    }
  },
  tasks: {
    addTask(task) {
      return instanceApi.post('/tasks', task).then(({ data }) => data);
    },

    deleteTask(taskId) {
      return instanceApi.delete(`/tasks/${taskId}`).then(({ data }) => data);
    },

    editNameTask(taskId, text) {
      return instanceApi.patch(`/tasks/${taskId}`, { text }).then(({ data }) => data);
    },

    changeCompleted(taskId, completed) {
      return instanceApi.patch(`/tasks/${taskId}`, { completed }).then(({ data }) => data);
    },

    filterTasksByCompleted(categoryId, completed) {
      return instanceApi.get(`/categories/${categoryId}/tasks?completed=${completed}`).then(({ data }) => data);
    },

    getTasksForCategory(categoryId) {
      return instanceApi.get(`/categories/${categoryId}/tasks`).then(({ data }) => data);
    }
  },
  colors: {
    getColors() {
      return instanceApi.get('/colors').then(({ data }) => data);
    }
  }
};
